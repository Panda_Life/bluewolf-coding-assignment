A disruptive new weather data startup, SkyCast Inc., reached out to you for your help with a
small development project. They are looking to change the way people read weather forecasts
and would like a stable base to iterate from.

APIs to Use
SkyCast is looking to leverage a weather API (https://developer.forecast.io) combined with
a geolocation API (https://developers.google.com/maps) to allow users to retrieve current
as well as historic data about any location that the users search.
